# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import znc with context %}

znc-container-clean-znc-container-absent:
  docker_container.absent:
    - name: {{ znc.container.name }}
    - force: True
