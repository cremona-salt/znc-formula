# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import znc with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = znc.docker.mountpoint %}

znc-container-running-container-present:
  docker_image.present:
    - name: {{ znc.container.image }}
    - tag: {{ znc.container.tag }}
    - force: True

znc-container-running-znc-container-managed:
  docker_container.running:
    - name: {{ znc.container.name }}
    - image: {{ znc.container.image }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/znc/config:/config
    - port_bindings:
      - 6501:6501
      - 6502:6502
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.znc-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.znc-web.middlewares=znc-redirect-websecure"
      - "traefik.http.routers.znc-web.rule=Host(`{{ znc.container.url }}`)"
      - "traefik.http.routers.znc-web.entrypoints=web"
      - "traefik.http.routers.znc-websecure.rule=Host(`{{ znc.container.url }}`)"
      - "traefik.http.routers.znc-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.znc-websecure.tls=true"
      - "traefik.http.routers.znc-websecure.entrypoints=websecure"
