# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import znc with context %}

znc-config-file-jq-package-absent:
  pkg.removed:
    - pkgs: {{ znc.pkgs.jq | yaml }}

znc-config-file-znc-extract-ssl-script-absent:
  file.absent:
    - name: /usr/sbin/extract_znc_letsencrypt_cert

znc-config-file-znc-extract-ssl-script-cronjob-absent:
  cron.absent:
    - name: /usr/sbin/extract_znc_letsencrypt_cert
