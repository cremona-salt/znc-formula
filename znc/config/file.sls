# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import znc with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = znc.docker.mountpoint %}

znc-config-file-jq-package-installed:
  pkg.installed:
    - pkgs: {{ znc.pkgs.jq | yaml }}

znc-config-file-znc-extract-ssl-script-installed:
  file.managed:
    - name: /usr/sbin/extract_znc_letsencrypt_cert
    - source: {{ files_switch(['extract_znc_letsencrypt_cert.tmpl'],
                              lookup='znc-config-file-znc-extract-ssl-script-installed',
                 )
              }}
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

znc-config-file-znc-extract-ssl-script-cronjob-managed:
  cron.present:
    - name: /usr/sbin/extract_znc_letsencrypt_cert
    - user: root
    - special: '@daily'
    - require:
      - znc-config-file-znc-extract-ssl-script-installed

{% for dir in [
  'docker/znc/config',
  ]
%}
znc-config-file-znc-config-{{ dir|replace('/', '-') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0750
    - file_mode: 0640
    - makedirs: True
{% endfor %}
